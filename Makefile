COMPOSE_FILE?=docker-compose.yml
COMPOSE_PROJECT_NAME=tote
COMPOSE=docker-compose -p $(COMPOSE_PROJECT_NAME) -f $(COMPOSE_FILE)

init: clean build composer-install

clean:
	$(COMPOSE) kill
	$(COMPOSE) rm --force

build:
	$(COMPOSE) pull
	$(COMPOSE) build

run:
	$(COMPOSE) run --rm tote

stop:
	$(COMPOSE) kill

composer-install:
	$(COMPOSE) run --rm composer -nov install

composer-update:
	$(COMPOSE) run --rm composer -nov update

# example:
# PACKAGE= slim/slim make composer-require
composer-require: PACKAGE?=
composer-require:
	$(COMPOSE) run --rm composer -nov require $(PACKAGE)

# example:
# PACKAGE=phpunit/phpunit make composer-require-dev
composer-require-dev: PACKAGE?=
composer-require-dev:
	$(COMPOSE) run --rm composer require --dev $(PACKAGE)

tests:
	$(COMPOSE) run --rm phpunit -c phpunit.xml

coverage:
	$(COMPOSE) run --rm phpunit -c phpunit.xml --coverage-html ./tests/reports

.PHONY: tests