<?php

use Tote\Cli\Tote;

require_once __dir__ . '/../vendor/autoload.php';

$tote = new Tote(15, 12, 18);

$tote->run();
