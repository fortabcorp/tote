<?php

namespace Tote\Cli;

use Tote\Products;
use Tote\Pools;

class Tote
{
    private $win;
    private $place;
    private $exacta;

    public function __construct($win_commission = 0, $place_commission = 0, $exacta_commission = 0)
    {
        $this->win = new Pools\Win($win_commission);
        $this->place = new Pools\Place($place_commission);
        $this->exacta = new Pools\Exacta($exacta_commission);
    }

    /**
     * Initiates stdin reading loop until it reads results
     *
     * @return  void
     */
    public function run()
    {
        $input = [];

        while ($line = fgets(STDIN)) {
            $line = trim($line);
            $input[] = $line;

            $parts = explode(':', $line);
            $type = $parts[0];

            if ($type === 'Result') {
                break;
            }
        }

        foreach ($input as $line) {

            $this->parse($line);
        }
    }

    /**
     * Parses a single line and either adds a bet to the appropriate pool, or
     * shows the results.
     *
     * @param   string  $line  Bet or Result line
     *
     * @return  void
     */
    private function parse(string $line)
    {
        $parts = explode(':', $line);
        $type = $parts[0];

        switch ($type) {
            case 'Bet':
                $product = $parts[1];
                array_shift($parts);

                $this->addBet($parts);

                return true;
            case 'Result':
                $winners = [ $parts[1], $parts[2], $parts[3] ];
                $this->results($winners);

                return false;
            default:
                return false;
        }

        return false;
    }

    /**
     * Adds a Bet to the appropriate pool
     *
     * @param  []mixed  $bet
     */
    private function addBet(array $bet): bool
    {
        switch ($bet[0]) {
            case 'W':
                $this->win->addBet(new Products\Win((int) $bet[1], (int) $bet[2]));
                break;
            case 'P':
                $this->place->addBet(new Products\Place((int) $bet[1], (int) $bet[2]));
                break;
            case 'E':
                $selections = explode(',', $bet[1]);
                $this->exacta->addBet(new Products\Exacta((int) $selections[0], (int) $selections[1], $bet[2]));
                break;
            default:
                return false;
        }

        return true;
    }

    /**
     * Shows the results of the bets in the pools
     *
     * @param   []mixed  $winners
     *
     * @return  void
     */
    public function results(array $winners)
    {
        $first = (int) $winners[0];
        $second = (int) $winners[1];
        $third = (int) $winners[2];

        $win = $this->win->calculate($first, $second, $third);

        echo sprintf("Win:%s:$%s\n", $win[0], $win[1]);

        $place = $this->place->calculate($first, $second, $third);

        foreach ($place as $p) {
            echo sprintf("Place:%s:$%s\n", $p[0], $p[1]);
        }

        $exacta = $this->exacta->calculate($first, $second, $third);

        echo sprintf("Exacta:%s,%s:$%s\n", $exacta[0], $exacta[1], $exacta[2]);
    }
}
