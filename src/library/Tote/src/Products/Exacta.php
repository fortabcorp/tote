<?php

namespace Tote\Products;

class Exacta extends Product
{
    /**
     * @param  int  $first   First winner
     * @param  int  $second  Second winner
     * @param  int  $stake   Stake for the bet
     */
    public function __construct(int $first = 0, int $second = 0, int $stake = 0)
    {
        $this->selections = [ $first, $second ];
        $this->stake = $stake;
    }
}
