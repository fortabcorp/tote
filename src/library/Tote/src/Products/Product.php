<?php

namespace Tote\Products;

abstract class Product
{
    /**
     * Winner selections for bet
     *
     * @var  []int
     */
    protected $selections = [];

    /**
     * How much is staked on the bet
     *
     * @var  int
     */
    protected $stake = 0;

    /**
     * Return the selections for this Product/Bet
     *
     * @return  []int
     */
    public function getSelections(): array
    {
        return $this->selections;
    }

    /**
     * Return the stake for this Product/Bet
     *
     * @return  int
     */
    public function getStake(): int
    {
        return $this->stake;
    }
}
