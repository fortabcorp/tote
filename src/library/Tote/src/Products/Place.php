<?php

namespace Tote\Products;

class Place extends Product
{
    /**
     * @param  int  $selection  Which winner is staked to Place
     * @param  int  $stake      Stake for the bet
     */
    public function __construct(int $selection = 0, int $stake = 0)
    {
        $this->selections = [ $selection ];
        $this->stake = $stake;
    }
}
