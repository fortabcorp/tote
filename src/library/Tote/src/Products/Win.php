<?php

namespace Tote\Products;

class Win extends Product
{
    /**
     * @param  int  $selection  Which winner
     * @param  int  $stake      Stake for the bet
     */
    public function __construct(int $selection = 0, int $stake = 0)
    {
        $this->selections = [ $selection ];
        $this->stake = $stake;
    }
}
