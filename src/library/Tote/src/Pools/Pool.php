<?php

namespace Tote\Pools;

use Tote\Products;

interface Pool
{
    /**
     * @param  int  $commission  Percent commission to remove from pool
     */
    public function __construct(int $commission = 0);

    /**
     * Add a bet to the pool
     *
     * @param   Products\Product  $bet
     *
     * @return  bool
     */
    public function addBet(Products\Product $bet): bool;

    /**
     * Calculate the dividends for the bets in the pool
     *
     * @param   int      $first   First winner
     * @param   int      $second  Second winner
     * @param   int      $third   Third winner
     *
     * @return  []mixed           Results of the calculation
     */
    public function calculate(int $first, int $second, int $third): array;
}
