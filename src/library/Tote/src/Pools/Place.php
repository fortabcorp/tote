<?php

namespace Tote\Pools;

use Tote\Products;

class Place implements Pool
{
    /**
     * Commission percentage
     *
     * @var  integer
     */
    private $commission = 0;

    /**
     * Pool of bets
     *
     * @var  []Products\Exacta
     */
    private $pool = [];

    /**
     * @inherit
     */
    public function __construct(int $commission = 0)
    {
        $this->commission = $commission;
    }

    /**
     * @inherit
     */
    public function addBet(Products\Product $bet): bool
    {
        if ($bet instanceof Products\Place === false) {
            return false;
        }

        $this->pool[] = $bet;

        return true;
    }

    /**
     * @inherit
     *
     * @return  []mixed  Example: [
     *                       [ 1, 1.93 ], // first winner, dividend
     *                       [ 3, 4.10 ], // second winner, dividend
     *                       [ 2, 0.43 ], // third winner, dividend
     *                   ]
     */
    public function calculate(int $first, int $second, int $third): array
    {
        $total = 0;
        $winning_proportions = [
            1 => 0,
            2 => 0,
            3 => 0,
        ];

        foreach ($this->pool as $bet) {
            $stake = $bet->getStake();

            $total += $stake;

            $selection = (int) current($bet->getSelections());

            if ($selection === $first) {
                $winning_proportions[1] += $stake;
            } else if ($selection === $second) {
                $winning_proportions[2] += $stake;
            } else if ($selection === $third) {
                $winning_proportions[3] += $stake;
            }
        }

        $calculated_commission = $this->commission / 100 * $total;
        $total -= $calculated_commission;
        $total /= 3;

        $dividends = [];

        if ($winning_proportions[1] === 0) {
            $dividends[1] = 0;
        } else {
            $dividends[1] = round($total / $winning_proportions[1], 2);
        }

        if ($winning_proportions[2] === 0) {
            $dividends[2] = 0;
        } else {
            $dividends[2] = round($total / $winning_proportions[2], 2);
        }

        if ($winning_proportions[3] === 0) {
            $dividends[3] = 0;
        } else {
            $dividends[3] = round($total / $winning_proportions[3], 2);
        }

        return [
            [ $first, $dividends[1] ],
            [ $second, $dividends[2] ],
            [ $third, $dividends[3] ],
        ];
    }
}
