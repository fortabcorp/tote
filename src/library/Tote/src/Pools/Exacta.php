<?php

namespace Tote\Pools;

use Tote\Products;

class Exacta implements Pool
{
    /**
     * Commission percentage
     *
     * @var  integer
     */
    private $commission = 0;

    /**
     * Pool of bets
     *
     * @var  []Products\Exacta
     */
    private $pool = [];

    /**
     * @inherit
     */
    public function __construct(int $commission = 0)
    {
        $this->commission = $commission;
    }

    /**
     * @inherit
     */
    public function addBet(Products\Product $bet): bool
    {
        if ($bet instanceof Products\Exacta === false) {
            return false;
        }

        $this->pool[] = $bet;

        return true;
    }

    /**
     * @inherit
     *
     * @return  []mixed  Example: [ 1, 2, 1.93 ] //first winner, second winner, dividend
     */
    public function calculate(int $first, int $second, int $third): array
    {
        $total = 0;
        $winning_proportion = 0;
        $winning_order = [ $first, $second ];

        foreach ($this->pool as $bet) {
            $total += $bet->getStake();

            if ($bet->getSelections() == $winning_order) {
                $winning_proportion += $bet->getStake();
            }
        }

        $calculated_commission = $this->commission / 100 * $total;
        $total -= $calculated_commission;

        if ($total === 0 || $winning_proportion === 0) {
            $dividend = 0;
        } else {
            $dividend = round($total / $winning_proportion, 2);
        }

        return [ $first, $second, $dividend ];
    }
}
