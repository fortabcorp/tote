<?php

namespace Tote\Pools;

use Tote\Products;

class Win implements Pool
{
    /**
     * Commission percentage
     *
     * @var  integer
     */
    private $commission = 0;

    /**
     * Pool of bets
     *
     * @var  []Products\Exacta
     */
    private $pool = [];

    /**
     * @inherit
     */
    public function __construct(int $commission = 0)
    {
        $this->commission = $commission;
    }

    /**
     * @inherit
     */
    public function addBet(Products\Product $bet): bool
    {
        if ($bet instanceof Products\Win === false) {
            return false;
        }

        $this->pool[] = $bet;

        return true;
    }

    /**
     * @inherit
     *
     * @return  []mixed  Example: [ 1, 12.55 ] //first winner, dividend
     */
    public function calculate(int $first, int $second, int $third): array
    {
        $total = 0;
        $winning_proportion = 0;

        foreach ($this->pool as $bet) {
            $total += $bet->getStake();

            if ((int) current($bet->getSelections()) === $first) {
                $winning_proportion += $bet->getStake();
            }
        }

        $calculated_commission = $this->commission / 100 * $total;
        $total -= $calculated_commission;

        if ($total === 0 || $winning_proportion === 0) {
            $dividend = 0;
        } else {
            $dividend = round($total / $winning_proportion, 2);
        }

        return [ $first, $dividend ];
    }
}
