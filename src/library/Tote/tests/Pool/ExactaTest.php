<?php

namespace Tote\Tests\Pools;

use PHPUnit\Framework\TestCase;
use Tote\Pools\Exacta;
use Tote\Products;

class ExactaTest extends TestCase
{
    /**
     * @dataProvider calculateProvider
     */
    public function testCalculate($commission, $bets, $winners, $expected)
    {
        $exacta = new Exacta($commission);

        foreach ($bets as $bet) {
            $exacta->addBet($bet);
        }

        $results = $exacta->calculate($winners[0], $winners[1], $winners[2]);

        $this->assertEquals($expected, $results);
    }

    public function calculateProvider()
    {
        $zeroCommission = [
            0,
            [
                new Products\Exacta(1, 2, 10),
                new Products\Exacta(1, 2, 20),
                new Products\Exacta(2, 1, 40),
            ],
            [1, 2, 3],
            [1, 2, 2.33],
        ];

        $fullCommission = [
            100,
            [
                new Products\Exacta(1, 2, 10),
                new Products\Exacta(1, 2, 20),
                new Products\Exacta(2, 1, 40),
            ],
            [1, 2, 3],
            [1, 2, 0],
        ];

        $normalCommission = [
            18,
            [
                new Products\Exacta(1, 2, 10),
                new Products\Exacta(1, 2, 20),
                new Products\Exacta(2, 1, 40),
            ],
            [1, 2, 3],
            [1, 2, 1.91],
        ];

        $example = [
            18,
            [
                new Products\Exacta(1, 2, 13),
                new Products\Exacta(2, 3, 98),
                new Products\Exacta(1, 3, 82),
                new Products\Exacta(3, 2, 27),
                new Products\Exacta(1, 2, 5),
                new Products\Exacta(2, 3, 61),
                new Products\Exacta(1, 3, 28),
                new Products\Exacta(3, 2, 25),
                new Products\Exacta(1, 2, 81),
                new Products\Exacta(2, 3, 47),
                new Products\Exacta(1, 3, 93),
                new Products\Exacta(3, 2, 51),
            ],
            [2, 3, 1],
            [2, 3, 2.43],
        ];

        return [
            $zeroCommission,
            $fullCommission,
            $normalCommission,
            $example,
        ];
    }
}
