<?php

namespace Tote\Tests\Pools;

use PHPUnit\Framework\TestCase;
use Tote\Pools\Place;
use Tote\Products;

class PlaceTest extends TestCase
{
    /**
     * @dataProvider calculateProvider
     */
    public function testCalculate($commission, $bets, $winners, $expected)
    {
        $place = new Place($commission);

        foreach ($bets as $bet) {
            $place->addBet($bet);
        }

        $results = $place->calculate($winners[0], $winners[1], $winners[2]);

        $this->assertEquals($expected, $results);
    }

    public function calculateProvider()
    {
        $zeroCommission = [
            0,
            [
                new Products\Place(1, 10),
                new Products\Place(1, 20),
                new Products\Place(2, 40),
            ],
            [1, 2, 3],
            [
                [1, 0.78],
                [2, 0.58],
                [3, 0],
            ],
        ];

        $fullCommission = [
            100,
            [
                new Products\Place(1, 10),
                new Products\Place(1, 20),
                new Products\Place(2, 40),
            ],
            [1, 2, 3],
            [
                [1, 0],
                [2, 0],
                [3, 0],
            ],
        ];

        $normalCommission = [
            12,
            [
                new Products\Place(1, 10),
                new Products\Place(1, 20),
                new Products\Place(2, 40),
            ],
            [1, 2, 3],
            [
                [1, 0.68],
                [2, 0.51],
                [3, 0],
            ],
        ];

        $example = [
            12,
            [
                new Products\Place(1, 31),
                new Products\Place(2, 89),
                new Products\Place(3, 28),
                new Products\Place(4, 72),
                new Products\Place(1, 40),
                new Products\Place(2, 16),
                new Products\Place(3, 82),
                new Products\Place(4, 52),
                new Products\Place(1, 18),
                new Products\Place(2, 74),
                new Products\Place(3, 39),
                new Products\Place(4, 105),
            ],
            [2, 3, 1],
            [
                [2, 1.06],
                [3, 1.27],
                [1, 2.13],
            ],
        ];

        return [
            $zeroCommission,
            $fullCommission,
            $normalCommission,
            $example,
        ];
    }
}
