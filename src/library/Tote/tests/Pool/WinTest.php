<?php

namespace Tote\Tests\Pools;

use PHPUnit\Framework\TestCase;
use Tote\Pools\Win;
use Tote\Products;

class WinTest extends TestCase
{
    /**
     * @dataProvider calculateProvider
     */
    public function testCalculate($commission, $bets, $winners, $expected)
    {
        $win = new Win($commission);

        foreach ($bets as $bet) {
            $win->addBet($bet);
        }

        $dividend = $win->calculate($winners[0], $winners[1], $winners[2]);

        $this->assertEquals($expected, $dividend);
    }

    public function calculateProvider()
    {
        $zeroCommission = [
            0,
            [
                new Products\Win(1, 10),
                new Products\Win(1, 20),
                new Products\Win(2, 40),
            ],
            [1, 2, 3],
            [1, 2.33],
        ];

        $fullCommission = [
            100,
            [
                new Products\Win(1, 10),
                new Products\Win(1, 20),
                new Products\Win(2, 40),
            ],
            [1, 2, 3],
            [1, 0],
        ];

        $normalCommission = [
            15,
            [
                new Products\Win(1, 10),
                new Products\Win(1, 20),
                new Products\Win(2, 40),
            ],
            [1, 2, 3],
            [1, 1.98],
        ];

        $example = [
            15,
            [
                new Products\Win(1, 3),
                new Products\Win(2, 4),
                new Products\Win(3, 5),
                new Products\Win(4, 5),
                new Products\Win(1, 16),
                new Products\Win(2, 8),
                new Products\Win(3, 22),
                new Products\Win(4, 57),
                new Products\Win(1, 42),
                new Products\Win(2, 98),
                new Products\Win(3, 63),
                new Products\Win(4, 15),
            ],
            [2, 3, 1],
            [2, 2.61],
        ];

        return [
            $zeroCommission,
            $fullCommission,
            $normalCommission,
            $example,
        ];
    }
}
