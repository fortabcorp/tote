# tote

## Dependencies

* docker (tested on version 1.12.1, build 23cf638)
* docker-compose (tested on version 1.8.0, build f3628c7)

## Building/Running

### Initial Setup

```
make init
```

This will clean up the docker environment, pull and build the required images, and tell npm to install packages.

### Running tests

```
make tests
```

This will run the test suite for the project.

### Running the CLI to accept STDIN input

```
make run
```

Paste the input and press enter. The results will be displayed immediately after.